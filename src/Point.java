class Point {
    public float x, y;

    public Point(float _x, float _y) {
        x = _x;
        y = _y;
    }

    public String toString(){
        return new String("Point: (" + x + "," + y + ")");
    }

    public static Point min(Point p1,Point p2){
        return new Point(Math.min(p1.x,p2.x),Math.min(p1.y,p2.y));
    }

    public static Point max(Point p1,Point p2){
        return new Point(Math.max(p1.x,p2.x),Math.max(p1.y,p2.y));
    }
}