public class RTree {
    int t;

    private class MBR{
        Point p1,p2;
        float area;

        MBR(Point x,Point y){
            p1 = Point.min(x,y);
            p2 = Point.max(x,y);
            area = (p2.y - p1.y) * (p2.x - p1.x);
        }

        MBR(Point x){
            this(x,x);
        }

        MBR(){
            this(new Point(0,0));
        }

        MBR extend(Point p){
            return new MBR(Point.min(p1,p),Point.max(p2,p));
        }
    }

    private class Node{
        Node[]child;
        MBR mbr;

        Node(){
            child = new Node[2 * t - 1];
            mbr = new MBR();
        }
    }

    private Node root;

    RTree(){
        root = null;
    }

    public void insert(Point p){

    }
}
